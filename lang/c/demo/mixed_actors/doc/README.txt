
LIDE C Mixed Actor Demo

This demo provides a simple example of a project that integrates user-defined
(custom) actors with actors from the LIDE-C library. The demo is referred to as
the LIDE Mixed-Actor Demo (LMAD), and accordingly, "lmad" is used as a naming
prefix for selected names.

To setup the files needed to build the project, including the testing driver
within the test/ tree:

cd src/
./buildsetup

This will create a directory called build/ in the top-level of
the project. The build/ directory contains files, including makefiles,
for building the project. 

Then the project (source code in src/ and test/) can be compiled any time
by running:

cd src/
./makeme

Building the project will produce a library (.a file) and executable
driver program in the bin/ directory of the project.
The library contains all of the functionality in the src/ directory in
a form that can be linked into main programs, such as testing drivers
or application programs.

To exercise the test suite after compiling the project:

cd test/
dxtest

In case you are experimenting with modifications to the project or using it as
a template for your own project: one needs to run ./buildsetup again only if
the build configuration changes (that is, if any of the CMakeList files in the
project change).  Otherwise, you just need to run src/makeme to update the
build after editing any source files.

