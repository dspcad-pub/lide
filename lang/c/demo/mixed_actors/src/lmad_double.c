/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2023
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


#include <stdio.h>
#include <stdlib.h>

#include "lide_c_fifo.h"
#include "lide_c_util.h"
#include "lmad_double.h"

struct _lmad_double_context_struct {
#include "lide_c_actor_context_type_common.h"
    /* Input ports. */
    lide_c_fifo_pointer in;

    /* Output port. */
    lide_c_fifo_pointer out;
};

lmad_double_context_type *lmad_double_new(
        lide_c_fifo_pointer in, lide_c_fifo_pointer out) {

    lmad_double_context_type *context = NULL;

    context = lide_c_util_malloc(sizeof(lmad_double_context_type));
    context->mode = LMAD_DOUBLE_MODE_PROCESS;
    context->enable =
            (lide_c_actor_enable_function_type)lmad_double_enable;
    context->invoke =
            (lide_c_actor_invoke_function_type)lmad_double_invoke;
    context->in = in;
    context->out = out;
    return context;
}

bool lmad_double_enable(
        lmad_double_context_type *context) {
    bool result = false;

    switch (context->mode) {
        case LMAD_DOUBLE_MODE_INACTIVE:
            result = false;
            break;
        case LMAD_DOUBLE_MODE_PROCESS:
            result = (lide_c_fifo_population(context->in) >= 1) &&
                     (lide_c_fifo_population(context->out) <
                      lide_c_fifo_capacity(context->out));
            break;
        default:
            result = false;
            break;
    }
    return result;
}

void lmad_double_invoke(lmad_double_context_type *context) {
    int num = 0;
    switch (context->mode) {
        case LMAD_DOUBLE_MODE_PROCESS:
            lide_c_fifo_read(context->in, &(num));
            num = num * 2;
            lide_c_fifo_write(context->out, &num);
            context->mode = LMAD_DOUBLE_MODE_PROCESS;
            break;
        case LMAD_DOUBLE_MODE_INACTIVE:
            context->mode = LMAD_DOUBLE_MODE_INACTIVE;
            break;
        default:
            context->mode = LMAD_DOUBLE_MODE_INACTIVE;
            break;
    }
    return;
}

void lmad_double_terminate(
        lmad_double_context_type *context) {
    free(context);
}