#ifndef _lmad_graph_h
#define _lmad_graph_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2023
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


#include "lide_c_basic.h"
#include "lide_c_util.h"
#include "lide_c_actor.h"
#include "lide_c_fifo.h"
#include "lide_c_graph.h"
#include "lide_c_file_source.h"
#include "lide_c_file_sink.h"

#include "lmad_increment.h"
#include "lmad_double.h"

#define BUFFER_CAPACITY 1024

/* An enumeration of the actors in this application. */
#define ACTOR_SOURCE1   0
#define ACTOR_SOURCE2   1
#define ACTOR_INCREMENT   2
#define ACTOR_DOUBLE   3
#define ACTOR_SINK1      4
#define ACTOR_SINK2      5

#define FIFO_SRC1_INC      0
#define FIFO_INC_SNK1      1
#define FIFO_SRC2_DBL      2
#define FIFO_DBL_SNK2       3
/* The total number of actors in the application. */
#define ACTOR_COUNT     6
#define FIFO_COUNT      4


/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with add objects. */
struct _lmad_graph_context_struct;
typedef struct _lmad_graph_context_struct
        lmad_graph_context_type;

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/

lmad_graph_context_type *lmad_graph_new(
        char *in_file1, char *in_file2, char *out_file1, char *out_file2);

void lmad_graph_terminate(
        lmad_graph_context_type *context);


#endif