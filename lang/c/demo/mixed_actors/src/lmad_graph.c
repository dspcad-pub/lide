/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2023
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lmad_graph.h"
#include "lide_c_basic.h"
#include "lide_c_actor.h"
#include "lide_c_file_source.h"
#include "lide_c_file_sink.h"

#define NAME_LENGTH 20

struct _lmad_graph_context_struct {
#include "lide_c_graph_context_type_common.h"
    char *in_file1;
    char *in_file2;
    char *out_file1;
    char *out_file2;
};

lmad_graph_context_type *lmad_graph_new(
        char *in_file1, char *in_file2, char *out_file1, char *out_file2){
    int token_size, i;

    lmad_graph_context_type *context = NULL;

    context = lide_c_util_malloc(sizeof(lmad_graph_context_type));
    context->actor_count = ACTOR_COUNT;
    context->actors = (lide_c_actor_context_type **)lide_c_util_malloc(
            context->actor_count * sizeof(lide_c_actor_context_type *));
    context->fifo_count = FIFO_COUNT;
    context->fifos = (lide_c_fifo_pointer *)lide_c_util_malloc(context->fifo_count *
                                                               sizeof(lide_c_fifo_pointer));
    context->descriptors = (char **)lide_c_util_malloc(
            context->actor_count * sizeof(char *));
    for(i = 0; i < context->actor_count; i++){
        context->descriptors[i] = (char *)lide_c_util_malloc(
                context->actor_count * sizeof(char));
    }
    strcpy(context->descriptors[ACTOR_SOURCE1],"source1");
    strcpy(context->descriptors[ACTOR_SOURCE2],"source2");
    strcpy(context->descriptors[ACTOR_INCREMENT],"increment");
    strcpy(context->descriptors[ACTOR_DOUBLE],"double");
    strcpy(context->descriptors[ACTOR_SINK1],"sink1");
    strcpy(context->descriptors[ACTOR_SINK2],"sink2");

    context->in_file1 = in_file1;
    context->in_file2 = in_file2;
    context->out_file1 = out_file1;
    context->out_file2 = out_file2;

    token_size = sizeof(int);
    for(i = 0; i<context->fifo_count; i++){
        context->fifos[i] = lide_c_fifo_new(BUFFER_CAPACITY, token_size);
    }
    /* Create and connect the actors. */
    i = 0;
    context->actors[ACTOR_SOURCE1] = (lide_c_actor_context_type
                                      *)(lide_c_file_source_new(context->in_file1,
                                                                context->fifos[FIFO_SRC1_INC]));

    context->actors[ACTOR_INCREMENT] = (lide_c_actor_context_type
                                      *)(lmad_increment_new(context->fifos[FIFO_SRC1_INC],
                                                                  context->fifos[FIFO_INC_SNK1]));

    context->actors[ACTOR_SINK1] = (lide_c_actor_context_type *)
            (lide_c_file_sink_new(context->out_file1,
                                  context->fifos[FIFO_INC_SNK1]));


    context->actors[ACTOR_SOURCE2] = (lide_c_actor_context_type
                                      *)(lide_c_file_source_new(context->in_file2,
                                                                context->fifos[FIFO_SRC2_DBL]));
    context->actors[ACTOR_DOUBLE] = (lide_c_actor_context_type
                                        *)(lmad_double_new(context->fifos[FIFO_SRC2_DBL],
                                                              context->fifos[FIFO_DBL_SNK2]));

    context->actors[ACTOR_SINK2] = (lide_c_actor_context_type *)
            (lide_c_file_sink_new(context->out_file2,
                                  context->fifos[FIFO_DBL_SNK2]));

    return context;
}

void lmad_graph_terminate(
        lmad_graph_context_type *context){
    int i;
    /* Terminate FIFO*/
    for(i = 0; i<context->fifo_count; i++){
        lide_c_fifo_free(context->fifos[i]);
    }

    /* Terminate Actors*/
    lide_c_file_source_terminate((lide_c_file_source_context_type * )
                                         context->actors[ACTOR_SOURCE1]);
    lmad_increment_terminate((lmad_increment_context_type *)
                                           (context->actors[ACTOR_INCREMENT]));
    lide_c_file_sink_terminate((lide_c_file_sink_context_type * )
                                       context->actors[ACTOR_SINK1]);

    lide_c_file_source_terminate((lide_c_file_source_context_type * )
                                         context->actors[ACTOR_SOURCE2]);
    lmad_double_terminate((lmad_double_context_type *)
                                     (context->actors[ACTOR_DOUBLE]));
    lide_c_file_sink_terminate((lide_c_file_sink_context_type * )
                                       context->actors[ACTOR_SINK2]);

    free(context->fifos);
    free(context->actors);
    free(context->descriptors);
    //free(context);

    return;

}


