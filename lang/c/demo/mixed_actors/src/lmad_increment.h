#ifndef _lmad_increment_h
#define _lmad_increment_h

/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2023
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/


#include "lide_c_actor.h"
#include "lide_c_fifo.h"

/* Actor modes */
#define LMAD_INCREMENT_MODE_PROCESS 1
#define LMAD_INCREMENT_MODE_INACTIVE 2

/*******************************************************************************
TYPE DEFINITIONS
*******************************************************************************/

/* Structure and pointer types associated with increment objects. */
struct _lmad_increment_context_struct;
typedef struct _lmad_increment_context_struct
        lmad_increment_context_type;

/*******************************************************************************
INTERFACE FUNCTIONS
*******************************************************************************/
lmad_increment_context_type *lmad_increment_new(
        lide_c_fifo_pointer in, lide_c_fifo_pointer out);

bool lmad_increment_enable(lmad_increment_context_type *context);

void lmad_increment_invoke(lmad_increment_context_type *context);

void lmad_increment_terminate(lmad_increment_context_type *context);

#endif