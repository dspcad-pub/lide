/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1997-2023
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "lide_c_fifo.h"
#include "lide_c_file_source.h"
#include "lide_c_file_sink.h"
#include "lide_c_inner_product.h"
#include "lide_c_util.h"

#define LIDE_C_INNER_PROD_SCHED_BASIC (0)
#define LIDE_C_INNER_PROD_SCHED_DIAG (1)
#define LIDE_C_INNER_PROD_SCHEDULER (LIDE_C_INNER_PROD_SCHED_BASIC)

#define BUFFER_CAPACITY 1024

/* An enumeration of the actors in this application. */
#define ACTOR_MSOURCE 0
#define ACTOR_XSOURCE 1
#define ACTOR_YSOURCE 2
#define ACTOR_INNER_PRODUCT 3
#define ACTOR_SINK 4

/* The total number of actors in the application. */
#define ACTOR_COUNT 5

/* An enumeration of the actors in this application. */
#define FIFO_MSOURCE_INNER_PROD   0
#define FIFO_XSOURCE_INNER_PROD   1
#define FIFO_YSOURCE_INNER_PROD     2
#define FIFO_INNER_PROD_SINK     3

/* The total number of edges in the application. */
#define FIFO_COUNT      4

/* 
    Usage: lide_c_inner_product_driver.exe m_file x_file y_file out_file
*/
int main(int argc, char **argv) {
    char *m_file = NULL; 
    char *x_file = NULL; 
    char *y_file = NULL; 
    char *out_file = NULL; 
    lide_c_actor_context_type *actors[ACTOR_COUNT];
    
    /* Connectivity: fifo1: (m, product), fifo2: (x, product); 
       fifo3: (y, product); fifo4: (product, out) 
    */
    lide_c_fifo_pointer fifos[FIFO_COUNT];
    
    int token_size = 0;
    int i = 0;
    int arg_count = 5;
    
    /* actor descriptors (for diagnostic output) */
    char *descriptors[ACTOR_COUNT] = {"msource", "xsource", "ysource", 
            "inner_product", "sink"};

    /* Check program usage. */
    if (argc != arg_count) {
        fprintf(stderr, "lide_c_inner_product_driver.exe error: arg count");
        exit(1);
    }   

    /* Open the input and output file(s). */
    i = 1;
    m_file = argv[i++]; 
    x_file = argv[i++]; 
    y_file = argv[i++]; 
    out_file = argv[i++];
    
    /* Create the buffers. */
    token_size = sizeof(int);
    fifos[FIFO_MSOURCE_INNER_PROD] =
            lide_c_fifo_new(BUFFER_CAPACITY, token_size);
    fifos[FIFO_XSOURCE_INNER_PROD] =
            lide_c_fifo_new(BUFFER_CAPACITY, token_size);
    fifos[FIFO_YSOURCE_INNER_PROD] =
            lide_c_fifo_new(BUFFER_CAPACITY, token_size);
    fifos[FIFO_INNER_PROD_SINK] =
            lide_c_fifo_new(BUFFER_CAPACITY, token_size);

    /* Create and connect the actors. */
    i = 0;
    actors[ACTOR_MSOURCE] = (lide_c_actor_context_type *)
            (lide_c_file_source_new(m_file, fifos[FIFO_MSOURCE_INNER_PROD]));
    actors[ACTOR_XSOURCE] = (lide_c_actor_context_type *)
            (lide_c_file_source_new(x_file, fifos[FIFO_XSOURCE_INNER_PROD]));
    actors[ACTOR_YSOURCE] = (lide_c_actor_context_type *)
            (lide_c_file_source_new(y_file, fifos[FIFO_YSOURCE_INNER_PROD]));
    actors[ACTOR_INNER_PRODUCT] = (lide_c_actor_context_type *)
            (lide_c_inner_product_new(fifos[FIFO_MSOURCE_INNER_PROD],
            fifos[FIFO_XSOURCE_INNER_PROD], fifos[FIFO_YSOURCE_INNER_PROD],
            fifos[FIFO_INNER_PROD_SINK]));
    actors[ACTOR_SINK] = (lide_c_actor_context_type *)
            (lide_c_file_sink_new(out_file, fifos[FIFO_INNER_PROD_SINK]));

    /* Execute the graph. */
    if (LIDE_C_INNER_PROD_SCHEDULER == LIDE_C_INNER_PROD_SCHED_BASIC) {
        lide_c_util_simple_scheduler(actors, ACTOR_COUNT, descriptors);
    }else if (LIDE_C_INNER_PROD_SCHEDULER == LIDE_C_INNER_PROD_SCHED_DIAG){
        lide_c_util_diag_scheduler(actors, ACTOR_COUNT, descriptors, fifos,
                FIFO_COUNT);
    }else{
        fprintf(stderr, "Invalid scheduler specified\n");
        exit(1);
    }

    /* Normal termination. */
    return 0;
}
